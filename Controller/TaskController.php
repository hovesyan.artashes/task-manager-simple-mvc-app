<?php
/**
 * Created by PhpStorm.
 * User: hovat
 * Date: 11/23/2019
 * Time: 11:38 AM
 */

require 'Controller.php';
require 'Models/Task.php';


class TaskController extends Controller
{

    private $pageTaskCount = 3;


    public function showTasks($page,$sort_field = null,$sort_by = null){

        if($sort_by == 'ASC'){
            $sort_by_next = 'DESC';
        }else{
            $sort_by_next = 'ASC';
        }

        $task_updated = FALSE;
        if(isset($_SESSION['task_created'])){
            $task_updated = 'create';
            unset($_SESSION['task_created']);
        }elseif(isset($_SESSION['task_updated'])){
            $task_updated = 'update';
            unset($_SESSION['task_updated']);
        }
        $task = new Task();
        $tasks = $task->getWithPaginate($this->pageTaskCount,$this->pageTaskCount*($page-1),$sort_field,$sort_by);
        $page_count = ceil($task->getCount()/$this->pageTaskCount);
        $this->view->render('tasks.php',['tasks' => $tasks, 'page_count' => $page_count,'current_page'=> $page, 'current_sort' => $sort_field, 'sort_by' => $sort_by, 'sort_by_next' => $sort_by_next,  'task_updated' => $task_updated]);
    }

    public function edit($id){
        $errors = [];
        $task = new Task();
        $task = $task->loadWithProperties(['id' => $id]);
        $data = ['login' => $task['login'],'email' => $task['email'],'task' => $task['task']];

        if(isset($_POST['submit'])){

            $completed = 0;
            if(isset($_POST['completed'])){
                $completed = 1;
            };

            $data = ['login' => $_POST['login'],'email' => $_POST['email'],'task' => $_POST['task'],'completed' => $completed, 'edited_by_admin' => 1];

            if(empty($data['login'])){
                $errors['login_error'] = "Login field can't be empty";
            }

            if(empty($data['email'])){
                $errors['email_error'] = "Email field can't be empty";
            }elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                $errors['email_error'] = "Email is not valid";
            }

            if(empty($data['task'])){
                $errors['task_error'] = "Task field can't be empty";
            }

            if(empty($errors)){
                $task = new Task();
                $task->payload = $data;
                $task->update($id);
                $_SESSION['task_updated'] = TRUE;
                header('location: /task');
            }
        }
        $data = ['post' => $data,'errors' => $errors];
        $this->view->render('task_edit_form.php',$data);
    }

    public function createTasks(){
        $errors = [];
        $data = ['login' => '','email' => '','task' => ''];

        if(isset($_POST['submit'])){
            $data = ['login' => $_POST['login'],'email' => $_POST['email'],'task' => $_POST['task']];

            if(empty($data['login'])){
                $errors['login_error'] = "Login field can't be empty";
            }

            if(empty($data['email'])){
                $errors['email_error'] = "Email field can't be empty";
            }elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                $errors['email_error'] = "Email is not valid";
            }

            if(empty($data['task'])){
                $errors['task_error'] = "Task field can't be empty";
            }

            if(empty($errors)){
                $task = new Task();
                $task->payload = $data;
                $task->store();
                $_SESSION['task_created'] = TRUE;
                header('location: /task');
            }
        }
        $data = ['post' => $data,'errors' => $errors];
        $this->view->render('task_form.php',$data);
    }
}