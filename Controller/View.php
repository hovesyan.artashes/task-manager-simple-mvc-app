<?php

/**
 * Created by PhpStorm.
 * User: hovat
 * Date: 11/23/2019
 * Time: 4:19 PM
 */
class View
{

    function render($view, $data = null)
    {
        if(is_array($data)) {
            extract($data);
        }

        include 'views/layout.php';
    }
}