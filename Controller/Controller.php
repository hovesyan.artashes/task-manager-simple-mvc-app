<?php

/**
 * Created by PhpStorm.
 * User: hovat
 * Date: 11/23/2019
 * Time: 4:18 PM
 */

require 'View.php';

class Controller {

    public $view;

    function __construct()
    {
        $this->view = new View();
    }

}