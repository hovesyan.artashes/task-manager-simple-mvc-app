<?php
/**
 * Created by PhpStorm.
 * User: hovat
 * Date: 11/23/2019
 * Time: 11:38 AM
 */

require 'Controller.php';
require 'Models/User.php';


class AdminController extends Controller
{

    public function login(){
        if(isset($_SESSION['user'])){
            header('location: /task');
        }
        $errors = [];
        $data = ['login' => '','password' => ''];

        if(isset($_POST['submit'])){
            $data = ['login' => $_POST['login'],'password' => ''];

            if(empty($data['login'])){
                $errors['login_error'] = "Login field can't be empty";
            }

            if(empty($errors)){
                $user = new User();
                $user = $user->loadWithProperties(['login' => $data['login'],'password' => $_POST['password']]);
                if($user && $user['is_admin']){
                    $_SESSION['user'] = $user;
                    header('location: /task');
                }
                else{
                    $errors['wrong_password'] = 'Wrong login or password';
                }
            }
        }
        $data = ['post' => $data,'errors' => $errors];
        $this->view->render('login.php',$data);
    }

    public function logout(){
        unset($_SESSION['user']);
        header('location: /task');
    }
}