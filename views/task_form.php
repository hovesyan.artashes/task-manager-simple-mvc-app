<div class="container">
    <div class="offset-4 col-4">
        <div class="mt-4">
            <form method="post">
                <div class="form-group">
                    <label for="loginInput">Login</label>
                    <input value="<?=$post['login'] ? $post['login'] : ''?>" name="login" type="text" class="form-control" id="loginInput" placeholder="Login">
                    <?php if(isset($errors['login_error'])){
                        ?><div class="alert alert-danger" role="alert">
                        <?=$errors['login_error']?>
                        </div><?php
                    } ?>
                </div>
                <div class="form-group">
                    <label for="emailInput">Email address</label>
                    <input value="<?=$post['email']?>" name="email" type="text" class="form-control" id="emailInput" aria-describedby="emailHelp" placeholder="Enter email">
                    <?php if(isset($errors['email_error'])){
                        ?><div class="alert alert-danger" role="alert">
                        <?=$errors['email_error']?>
                        </div><?php
                    } ?>
                </div>
                <div class="form-group">
                    <label for="taskInput">Task</label>
                    <input value="<?=$post['task']?>" name="task" type="text" class="form-control" id="taskInput"  placeholder="Task text">
                    <?php if(isset($errors['task_error'])){
                        ?><div class="alert alert-danger" role="alert">
                        <?=$errors['task_error']?>
                        </div><?php
                    } ?>
                </div>
                <input type="submit" name="submit" class="btn btn-primary" value="Submit">
            </form>
        </div>
    </div>
</div>