<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>TaskManager</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/task">Navbar</a>
    <div class="collapse navbar-collapse ml-auto" id="navbarText">
        <?php if(isset($_SESSION['user'])){ ?>
            <a class="ml-auto" href="/admin/logout">Logout</a>
        <?php }else{ ?>
            <a class="ml-auto" href="/admin/login">Login</a>
        <?php } ?>
    </div>
</nav>
<?php include 'views/'.$view; ?>
</body>
</html>