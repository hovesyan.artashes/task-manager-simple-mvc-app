<div class="container">
    <div class="offset-2 col-8">
        <?php if($task_updated == 'create'){ ?>
            <div class="alert alert-success" role="alert">
                Task Successfully created
            </div>
        <?php }elseif($task_updated == 'update') { ?>
            <div class="alert alert-success" role="alert">
                Task was updated
            </div>
        <?php } ?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><a href="/task?page=<?=$current_page?>&sort_field=login&sort_by=<?= $sort_by_next ?>">Login</a></th>
                <th scope="col"><a href="/task?page=<?=$current_page?>&sort_field=email&sort_by=<?= $sort_by_next ?>">Email</a></th>
                <th scope="col"><a href="/task?page=<?=$current_page?>&sort_field=task&sort_by=<?= $sort_by_next ?>">Task</a></th>
                <th scope="col"><a href="/task?page=<?=$current_page?>&sort_field=completed&sort_by=<?= $sort_by_next ?>">Status</a></th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($tasks as $task){ ?>
                <tr>
                    <th scope="row"><?=$task['id']?></th>
                    <td><?=$task['login']?></td>
                    <td><?=$task['email']?></td>
                    <td><?=$task['task']?></td>
                    <td><?php if($task['completed']){?>Completed<?php } else { ?>In process<?php } if($task['edited_by_admin']){?>(edited by admin)<?php } ?></td>
                    <?php if(isset($_SESSION['user'])){?>
                        <td><a href="/task/edit?id=<?=$task['id']?>">Edit</a></td>
                    <?php } ?>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <?php for ($i = 1;$i <= $page_count; $i++){ ?>
                    <li class="page-item <?php if($i == $current_page){ ?>active<?php } ?> "><a class="page-link" href="?page=<?=$i?><?php if(isset($current_sort)){ ?>&sort_field=<?=$current_sort?>&sort_by=<?=$sort_by?><?php }?>"><?=$i?></a></li>
                <?php } ?>
            </ul>
        </nav>
        <a href="/task/create"><button type="button" class="btn btn-primary  btn-lg btn-block">Create Task</button></a>
    </div>
</div>