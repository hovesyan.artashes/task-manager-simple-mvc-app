<div class="container">
    <div class="offset-4 col-4">
        <div class="mt-4">
            <?php if(isset($_SESSION['login_required'])){
                ?><div class="alert alert-warning" role="alert">Please log in</div><?php
            } ?>
            <form method="post">
                <div class="form-group">
                    <label for="loginInput">Login</label>
                    <input value="<?=$post['login'] ? $post['login'] : ''?>" name="login" type="text" class="form-control" id="loginInput" placeholder="Login">
                    <?php if(isset($errors['login_error'])){
                        ?><div class="alert alert-danger" role="alert">
                           <?=$errors['login_error']?>
                        </div><?php
                    } ?>
                </div>
                <div class="form-group">
                    <label for="passwordInput">Password</label>
                    <input value="<?=$post['password']?>" name="password" type="password" class="form-control" id="passwordInput" aria-describedby="emailHelp" placeholder="Enter password">
                    <?php if(isset($errors['wrong_password'])){
                        ?><div class="alert alert-danger" role="alert">
                        <?=$errors['wrong_password']?>
                        </div><?php
                    } ?>
                </div>
                <input type="submit" name="submit" class="btn btn-primary" value="Login">
            </form>
        </div>
    </div>
</div>