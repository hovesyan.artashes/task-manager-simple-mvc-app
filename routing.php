<?php
/**
 * Created by PhpStorm.
 * User: hovat
 * Date: 11/23/2019
 * Time: 4:03 PM
 */
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
switch($path)
{
    case "/" :
        header('location: /task');
        break;
    case "/task" :
        $page = 1;
        $sort_field = null;
        $sort_by = null;
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }
        if(isset($_GET['sort_field'])){
            $sort_field = $_GET['sort_field'];
        }
        if(isset($_GET['sort_by'])){
            $sort_by = $_GET['sort_by'];
        }
        require_once("Controller/TaskController.php");
        $taskController = new TaskController;
        $taskController->showTasks($page,$sort_field,$sort_by);
        break;
    case "/task/create" :
        require_once("Controller/TaskController.php");
        $taskController = new TaskController;
        $taskController->createTasks();
        break;
    case "/task/edit" :
        if(!isset($_SESSION['user'])){
            $_SESSION['login_required'] = TRUE;
            header('location: /admin/login');
            die;
        }
        $page = 1;
        if(!isset($_GET['id'])){
            header('location: /task');
            die;
        }
        require_once("Controller/TaskController.php");
        $taskController = new TaskController;
        $taskController->edit($_GET['id']);
        break;
    case "/admin/login" :
        require_once("Controller/AdminController.php");
        $adminController = new AdminController;
        $adminController->login();
        break;
    case "/admin/logout" :
        require_once("Controller/AdminController.php");
        $adminController = new AdminController;
        $adminController->logout();
        break;
    default :
        require_once("views/page404.php");
        break;
}