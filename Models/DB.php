<?php

/**
 * Created by PhpStorm.
 * User: hovat
 * Date: 11/24/2019
 * Time: 4:30 PM
 */

class DB
{
    private static $connection;


    public static function connect($host, $user, $password, $database)
    {
        if (!isset(self::$connection))
        {
            return self::$connection = new mysqli($host, $user, $password, $database);
        }
        return self::$connection;
    }

}