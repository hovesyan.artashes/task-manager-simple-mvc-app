<?php

/**
 * Created by PhpStorm.
 * User: hovat
 * Date: 11/23/2019
 * Time: 7:37 PM
 */

require_once 'DB.php';

class Model
{

    protected $table = '';

    public $payload = [];

    public function __construct()
    {
        $this->conn = Db::connect("localhost", "root", "", "task_app");
    }

    public function store(){

        $fields = implode(',',array_keys($this->payload));
        $values = "'".reset($this->payload)."'";
        foreach (array_slice($this->payload,1) as $value){
            $values = $values.",'".$value."'";
        }

        $sql = "INSERT INTO `$this->table` ($fields)
        VALUES ($values)";


        if ($this->conn->query($sql) === TRUE) {
            return TRUE;
        }

        return FALSE;
    }

    public function update($id){
        $query = '';
        foreach ($this->payload as $key => $value){
            $query = $query."$key = '".$value."',";
        }
        $last_key = array_key_last($this->payload);
        $query = $query."$last_key = '".end($this->payload)."'";

        $sql = "UPDATE `$this->table` SET $query WHERE `id`=$id";

        if ($this->conn->query($sql) === TRUE) {
            return TRUE;
        }

        return FALSE;
    }

    public function getWithPaginate($limit,$offset,$sort_by = null,$asc = 'ASC'){

        $responseArr = [];
        $sql = "SELECT * FROM `$this->table`";
        if($sort_by){
            $sql = $sql." ORDER BY `$sort_by` $asc";
        }
        $sql = $sql." LIMIT $limit OFFSET $offset";
        $res = $this->conn->query($sql);
        if ($res) {
            while($row = $res->fetch_assoc()) {
                $responseArr[] = $row;
            }
            return $responseArr;
        }
        return FALSE;
    }

    public function getCount(){

        $sql = "SELECT COUNT(*) FROM `$this->table`";
        $res = $this->conn->query($sql);
        $row = $res->fetch_assoc();
        return $row['COUNT(*)'];
    }

    public function loadWithProperties(array $properties){
        $query = '';
        $allProperties = $properties;
        array_pop($properties);
        foreach ($properties as $key => $value){
            $query = $query."$key = '".$value."' and ";
        }
        $last_key = array_key_last($allProperties);
        $query = $query."$last_key = '".end($allProperties)."'";

        $sql = "SELECT * FROM $this->table WHERE ".$query;

        $res = $this->conn->query($sql);
        $row = $res->fetch_assoc();
        return $row;
    }

}